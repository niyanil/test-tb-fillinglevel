package com.ophardt.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TbFillingLevelApplication {

    public static void main(String[] args) {
        SpringApplication.run(TbFillingLevelApplication.class, args);
    }

}
