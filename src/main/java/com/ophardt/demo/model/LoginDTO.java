package com.ophardt.demo.model;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Getter
@Component
public class LoginDTO {

    @Value("${thingsboard.user}")
    private String username;

    @Value("${thingsboard.password}")
    private String password;



}
