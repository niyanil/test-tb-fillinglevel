package com.ophardt.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeSeriesDataDTO {
    private Long ts;
    private String value;

    public void TimeSeriesDataDTO(Long ts, String value){
        this.ts = ts;
        this.value = value;
    }

}
