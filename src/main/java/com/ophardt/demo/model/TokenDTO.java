package com.ophardt.demo.model;

import lombok.Data;

@Data
public class TokenDTO {

    private String token;
    private String refreshToken;
}
