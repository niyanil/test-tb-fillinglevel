package com.ophardt.demo.service;

import com.ophardt.demo.helper.HttpClient;
import com.ophardt.demo.helper.UrlBuilder;
import com.ophardt.demo.model.LoginDTO;
import com.ophardt.demo.model.TimeSeriesDataDTO;
import com.ophardt.demo.model.TokenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

@Component
public class DemoService {

    @Autowired
    HttpClient httpClient;
    @Autowired
    LoginDTO loginDTO;
    @Autowired
    UrlBuilder urlBuilder;
    String bearerToken;


    @PostConstruct
    public void init(){
        try {
            TokenDTO tokenDTO = httpClient.post(urlBuilder.getThingsBoardAuthTokenUrl(),loginDTO, null,TokenDTO.class);
            bearerToken = "Bearer "+tokenDTO.getToken();
        } catch (Exception e) { throw new IllegalStateException("Token cannot be retreived"); }
    }

    public List<TimeSeriesDataDTO> getActivationList(String entityGroupId) throws Exception {
        TimeSeriesDataDTO lastBottleInTimeSeries = getLatestTimeSeries(entityGroupId);
        Calendar cal = Calendar.getInstance();
        HashMap<String, Object> queryparam = new HashMap<>();
        queryparam.put("keys", "ACTIVATION");
        queryparam.put("startTs", lastBottleInTimeSeries.getTs());
        queryparam.put("endTs", cal.getTimeInMillis());
        return httpClient.getList(urlBuilder.getThingsBoardTelemetryDataUrl(entityGroupId), bearerToken, queryparam, TimeSeriesDataDTO.class);
    }

    public TimeSeriesDataDTO getLatestTimeSeries(String entityGroupId) throws Exception {
        HashMap<String, Object> queryparam = new HashMap<>();
        queryparam.put("keys", "BOTTLEIN");
        return httpClient.get(urlBuilder.getThingsBoardTelemetryDataUrl(entityGroupId), bearerToken, queryparam, TimeSeriesDataDTO.class);
    }
}
