package com.ophardt.demo.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ophardt.demo.helper.GenericList;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HttpClient {

    public String getAuthConstant(){
        return "X-Authorization";
    }

    public Client getHttpClient() throws Exception {
        return getUnsecureClient();
    }

    public <T,G> G post(String url, T body, String token,Class<G> type) throws Exception {
        return post(url,body,token).readEntity(type);
    }

    public <T> Response post(String url, T body, String token) throws Exception{
        Client authClient = getHttpClient();
        ObjectMapper mapper = new ObjectMapper();
        Invocation.Builder builder = authClient.target(url).request();
        if(token!=null){
            builder.header(getAuthConstant(),token);
        }
        Response response = builder.post(Entity.entity(mapper.writeValueAsString(body), MediaType.APPLICATION_JSON));
        return response;
    }

    public <T> List<T> getList(String url, String token, HashMap<String,Object> params, Class<T> type) throws Exception {
        return get(url,token, params).readEntity(GenericList.getListType(type));
    }

    public <T> T get(String url, String token, HashMap<String,Object> params, Class<T> type) throws Exception {
        return get(url,token, params).readEntity(type);
    }

    public Response get(String url, String token, HashMap<String, Object> params) throws Exception {
        Client authClient = getHttpClient();
        WebTarget webTarget = authClient.target(url);
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                webTarget = webTarget.queryParam(entry.getKey(), entry.getValue());
            }
        }
        Invocation.Builder builder = webTarget.request();
        if (token != null) {
            builder.header(getAuthConstant(), token);
        }
        Response response = builder.get();
        return response;
    }

    public static Client getUnsecureClient() throws Exception {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, new TrustManager[]{new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }}, new java.security.SecureRandom());
        HostnameVerifier allowAll = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        return ClientBuilder.newBuilder().sslContext(sslcontext).hostnameVerifier(allowAll).build();
    }
}
