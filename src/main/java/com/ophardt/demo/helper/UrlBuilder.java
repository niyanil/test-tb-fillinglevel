package com.ophardt.demo.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Component
public class UrlBuilder {

    @Autowired
    DemoConfiguration demoConfiguration;

    public String getThingsBoardAuthTokenUrl(){
        return buildUrl(getThingsBoardBaseUrl(), demoConfiguration.getLoginUrl());
    }

    public String getThingsBoardTelemetryDataUrl(String parameter) {
        return buildUrlWithSinglePathParam(getThingsBoardBaseUrl(), demoConfiguration.getTelemetryUrl(), parameter);
    }

    private String getThingsBoardBaseUrl() {
        return getBaseUrl(demoConfiguration.getProtocol(), demoConfiguration.getHost(), demoConfiguration.getRootUrl());
    }

    private static String buildUrlWithSinglePathParam(String baseUrl, String relativeUrl, String input){
        Map<String, String> parameters = new HashMap<>();
        parameters.put("inputValue", input);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(buildUrl(baseUrl, relativeUrl));
        URI formattedUri = builder.buildAndExpand(parameters).toUri();
        return formattedUri.toString();
    }

    private static String buildUrl(String baseUri, String relativeUri) {
        return baseUri + relativeUri;
    }

    private static String getBaseUrl(String protocol, String host, String rootURL) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(protocol).host(host).path(rootURL).build();
        return uriComponents.toString();
    }
}
