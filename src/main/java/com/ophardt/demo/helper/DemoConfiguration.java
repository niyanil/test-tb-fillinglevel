package com.ophardt.demo.helper;

import lombok.Data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class DemoConfiguration {

    @Value("${thingsboard.protocol}")
    private String protocol;
    @Value("${thingsboard.host}")
    private String host;
    @Value("${thingsboard.rootURL}")
    private String rootUrl;
    @Value("${thingsboard.path.login}")
    private String loginUrl;
    @Value("${thingsboard.path.devicesbyId}")
    private String telemetryUrl;




}
