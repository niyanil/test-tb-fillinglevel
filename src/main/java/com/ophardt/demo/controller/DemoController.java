package com.ophardt.demo.controller;

import com.ophardt.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ws.rs.core.Response;

@RestController
public class DemoController {

    @Autowired
    DemoService demoService;

    @RequestMapping("/")
    public Response index() throws Exception {
        return Response.ok(demoService.getActivationList("d51f5900-786a-11eb-9d1a-350357ffc282")).build();
    }
}
